'use strict';

/**
* Main Server
*/

var express = require('express')
var logger = require('./logger');
var errorhandler = require('errorhandler');
var config = require('./config');
var error = require('./error');
var except = require('./exception');
var db = require('./db');
var routeMiddleware = require('../middleware/route.js');
var bodyParser = require('body-parser');
var sugar = require('sugar');

var app, router, validMethods, port,
    mongoose;

app = express();
router = express.Router();
port = config.platform.port;
mongoose = db.mongoose();

// valid HTTP methods the express router is setup to handle
validMethods = [ 'get', 'post', 'put', 'delete' ];

if (process.env.NODE_ENV === 'development') {
	app.use(errorhandler({
            dumpExceptions  : true,
            showStack       : true
        }));
}

// Use Express Body Parser
app.use(bodyParser.json());


// For Public API Calls
app.use('/:type/:version/:model/:method/', routeMiddleware());

app.listen(port, function() {
    logger.info('Webserver started, Listening on port '+port);
});

app.get('/abc', function(req, res){
  res.send('Hello World\n');
});