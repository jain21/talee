/**
* Initializing All Version Model Schema
*/

var fs = require('fs');

var Models = {};

Models.loadAll = function() {

    var model_path = __dirname + '/../app/api/version';

    fs.readdirSync(model_path).forEach(function(dir){
        var path = model_path + '/' + dir;
        if(fs.lstatSync(path).isDirectory()){
            path += '/' + 'model';

            fs.readdirSync(path).forEach(function(d){
                var dir = path + '/' + d;

                if(fs.lstatSync(dir).isDirectory()) {
                    fs.readdirSync(dir).forEach(function(file) {
                        if(~file.indexOf('.js'))
                            require(dir + '/' + file)();
                    });
                }
            });
        }
    })
}

module.exports = Models;
