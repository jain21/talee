/**
 * Winston Logger
 */

var logger = require('winston');

var customLevels = {
    levels: {
        debug: 0,
        info: 1,
        warn: 2,
        error: 3
    },
    colors: {
        debug: 'cyan',
        info: 'green',
        warn: 'yellow',
        error: 'red'
    }
};

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'debug',
    levels: customLevels.levels,
    colorize: true,
    timestamp: true
});

logger.setLevels(customLevels.levels);
logger.addColors(customLevels.colors);

module.exports = logger;