'use strict';

/**
* Version Type
* @type: version
*/

var url = require('url');
var oauth = require(__dirname + '/controller/oauth/oauth');
var logger = require('winston');
var async = require('async');
var error = require(__dirname + '/../../../../lib/error');
var exception = require(__dirname + '/../../../../lib/exception');

module.exports = {
    route : routeRequest
}

/**
* Module Route Incoming Request
*/
function routeRequest(request, response, cb) {
    var m, path, model, met, data = {};

    m = request.params.model;
    met = request.params.method;
    met = (met.indexOf('.')) ? met.split('.')[0] : met; // splitting the JSON.from the end
    data = {
        body : request.body,
        access_token : request.query['access_token']
    };

    path = __dirname + '/controller/'+ m + '/' + m;


    try {
        model = require(path);
        if(model[met] === undefined) {
            throw new Error('Invalid Request: ' + met);
        }
    } catch(e) {
        logger.error(e + ' ');
        var err = error['invalid-request'];
        response.writeHead(err.status, { 'Content-Type' : 'application/json' });
        response.end(JSON.stringify(err));
        return;
    }

    access_token_validate(m, met, data.access_token,
                        function(err, found){
        if(err || !found) {
            response.end(JSON.stringify(err));
            return;
        }

        model[met](data, function(err, usr) {
            if(err){
                cb(err);
                return;
            }
            cb(null, usr);
        });
    });
}


/**
*   Validating the Access Token
**/
function access_token_validate(model, method, access_token, callback) {
    async.waterfall([
        function (cb) {
            if(exception[model].find(method)) {
                // not an error but stop propating
                // since method does not require
                // access_token
                cb({'not-required' : 'true'});
            } else {
                cb(null, false, access_token);
            }
        },
        function (method_found, access_token, cb) {
            if(access_token == '' || !access_token && !method_found) {
                cb(error['no-access-token']);
                return;
            }
            cb(null, access_token);
        },
        function(token, cb) {
            oauth.validate(access_token, function(err, found){
                if(err){
                    cb(err);
                    return;
                }
                cb(null, true);
            })
        }
    ], function(err, result){
        if(err){
            (err['not-required']) ? callback(null, true) : callback(err);
            return;
        }
        callback(null, result);
    });
}