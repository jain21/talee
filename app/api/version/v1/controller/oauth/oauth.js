'use strict';

/**
* User Authorization
*/

var crypto = require('crypto');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var error = require(__dirname + '/../../../../../../lib/error');

module.exports = {
    hash_password : _genHashPassword,
    salt : _generateSalt,
    access_token : _genAccessToken,
    validate : validateAccessToken,
    authorize : authorizeUser,
    find : findUser
}

/**
*   Validating the Access Token against User Model
**/
function validateAccessToken(access_token, cb) {
    User.findOne({ 'accessToken' : access_token }, function(err, found) {
        if(err){
            cb(error['db-error']);
            throw new Error(err);
            return;
        }
        if(found && Object.size(found)) {
            cb(null, true);
        } else {
            cb(error['invalid-access-token']);
        }
    });
}

/**
**   Authorize User for SignIn
**/
function authorizeUser(aData, cb) {

    var data = aData.body;
    User.find({ 'username' : data.username }, function(err, user) {
        if(err){
            cb(error['db-error']);
            throw new Error(err);
            return;
        }
        var user_password,  user_salt,
            gen_hash_password, username, email;

        if(Object.size(user)){
            user_password = user[0].password;
            user_salt = user[0].salt;
            gen_hash_password = _genHashPassword(data.password, user_salt);
            username = user[0].username;
            email = user[0].email;
        }

        if((data.username === username || data.username === email) &&
                gen_hash_password === user_password) {
            cb(null, error.success);
        } else {
            cb(error.unauthorized);
        }
    });
}

/**
*   Get User Info by Access Token
*   Only App wide scope. not for public API
**/
function findUser(access_token, cb) {
    var query;

    query = User.find({ 'accessToken' : access_token });
    query.select('firstName lastName username');
    query.exec(function(err, res) {
        if(err){
            cb(err);
            return;
        }
        cb(null, res);
    });
}

/**
*   Relating to Password + Hash + Authcode + Salt
**/
function _genAccessToken(data) {
    var random = _generateRandom(36322632);
    return crypto.createHash('sha256').update(data.username + random + Date.now()).digest('hex')
            .substring(3, 20);
}

function _genHashPassword(pass, salt) {
    return crypto.createHash('sha512').update(pass + salt, 'ascii').digest('base64');
}

function _generateSalt(data) {
    var pass = data.password;
    return crypto.createHash('md5').update(pass + Date.now(), 'ascii').digest('hex')
            .substring(5, 25);
}

function _generateRandom(number) {
    return Math.floor(Math.random() * number) + 1;
}
