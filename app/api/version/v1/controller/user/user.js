'use strict';

/**
* User Model
*/

var mongoose = require('mongoose');
var User = mongoose.model('User');
var oauth = require(__dirname + '/../oauth/oauth');
var error = require(__dirname + '/../../../../../../lib/error');

module.exports = {
    create : createUser,
    update : updateUserInfo,
    search : searchByUsername
}

/**
* Public API Functions
**/

/**
*  Create a New User
**/
function createUser(uData, cb) {
    var data = uData.body;
    var salt = oauth.salt(data);
    var   doc = {
        'firstName'   : data.firstName,
        'lastName'    : data.lastName,
        'username'    : data.username,
        'email'       : data.email,
        'password'    : oauth.hash_password(data.password, salt),
        'salt'        : salt,
        'groupId'     : data['groupId'] || [],
        'roles'       : data.role || ['regular'],
        'accessToken' : oauth.access_token(data)
    };

    var user = new User(doc);
    user.save(function(err, result) {
        if(err){
            // to prevent duplication
            (err.code === 11000) ? cb(error['username-exists'])
                                : cb(dbError(err));
            return;
        }

        var body = {
            id : result._id,
            fullname : result['firstName'] + ' ' + result['lastName'],
            username : result.username,
            role : result.roles,
            access_token : result['accessToken']
        };
        cb(null, success(body));
    });
}

/**
*  Search User by username
**/
function searchByUsername(sdata, cb) {
    var data, query;

    data = sdata.body;
    query = User.find({ 'username' : new RegExp('^' + data.q + '*', "i") });
    query.limit(10);
    query.select('firstName lastName username')
    query.exec(function(err, usr) {
        if(err){
            cb(dbError(err));
            throw new Error(err);
            return;
        }
        cb(null, success(usr));
    });
}

/**
*  Update User password/basic info
**/
function updateUserInfo(uData, callback) {
    var access_token, data;

    access_token = uData.access_token;
    data = uData.body;

    User.find({ 'accessToken' : access_token }, function(err, user) {
        if(err){
            cb(dbError(err));
            throw new Error(err);
            return;
        }
        var userid, newSalt, newHashPass, update;
        userid = user[0]._id;
        newSalt = oauth.salt(data);
        newHashPass = oauth.hash_password(data.password, newSalt);
        update = {  'firstName' : data.firstName,
                    'lastName' : data.lastName,
                    'password' : newHashPass,
                    'salt'    : newSalt
                 };
        User.update({ _id : userid }, update, function(err, upd){
            if(err){
                callback(dbError(err));
                throw new Error(err);
                return;
            }
            callback(null, success(error.success));
        });
    });
}


/**
* All Users
*
* @return {Array}
* @api private
*/
function getAllUsers(cb) {

    User.find({}, function(err, usr) {
        if(err){
            cb(dbError(err));
            throw new Error(err);
            return;
        }
        cb(null, success(usr));
    });
}


/**
* Local Functions
*/
function success(body) {
    var result = {};
    result.status = error.success;
    result.body = body;

    return result;
}

function dbError(err) {
    var result = {};
    result.status = error['db-error'];
    result.body = err;

    return result;
}