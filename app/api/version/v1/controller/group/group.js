'use strict';

/**
* Group Model
*/

var mongoose = require('mongoose');
var Group = mongoose.model('Group');
var User = mongoose.model('User');
var oauth = require(__dirname + '/../oauth/oauth');
var error = require(__dirname + '/../../../../../../lib/error');
var async = require('async');

module.exports = {
    create : createGroup
}

/**
* Public API Functions
**/

/**
*  Create a New Group
*  Make the group creator admin +
*  Add the GroupId to user's GroupId db
**/
function createGroup(gData, callback) {
    var data = gData.body;

    async.waterfall([
        function(cb) {
            oauth.find(gData.access_token, function(err, res) {
                if(err) {
                    cb(err);
                    return;
                }
                cb(null, res);
            });
        },
        function(u, cb){
            var userList = [{
                user : u[0]._id,
                roles : ['regular', 'admin']
            }];
            var doc = {
                name : gData.body.name,
                meta : {
                    createdBy : u[0]._id
                },
                users : userList
            };
            var group = new Group(doc);
            group.save(function(err, res) {
                if(err) {
                    cb(err);
                    return;
                }
                cb(null, res, u);
            });
        },
        function(groupInfo, userInfo, cb) {
            User.update({ _id : userInfo[0]._id }, { $addToSet : { groupId : groupInfo._id } }, function(err, res) {
                if(err){
                    cb(err);
                    return;
                }
                cb(null, groupInfo);
            });
        }
    ],
    function(err, result) {
            if(err){
                (err.code === 11000) ? callback(error['name-exists'])
                                    : callback(dbError(e));
                return;
            }
            callback(success(result));
    });

}

/**
* Local Functions
*/
function success(body) {
    var result = {};
    result.status = error.success;
    result.body = body;

    return result;
}

function dbError(err) {
    var result = {};
    result.status = error['db-error'];
    result.body = err;

    return result;
}