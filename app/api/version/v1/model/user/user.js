'use strict';

/**
* User Model
*/

var mongoose = require('mongoose');
var logger = require('winston');

module.exports = function() {

    var mongooseSchema, schema, User;

    mongooseSchema = mongoose.Schema;
    schema = require(__dirname + '/schema/user').schema;
    User = new mongooseSchema(schema);

    /**
    ** Methods
    */
    User.methods = {
        test : function test() {
            return "test";
        }
    }

    mongoose.model('User', User);

    if(process.env.NODE_ENV !== 'test'){
        logger.info('Successfully Initialised the User Schema');
    }
};