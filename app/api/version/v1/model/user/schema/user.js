'use strict';

/**
* User Schema Document
*/

module.exports.schema = {

    'firstName' : { type: String, required: true, trim: true },
    'lastName'  : { type: String, required: true, trim: true },
    'username'   : { type: String, required: true, trim: true, unique: true },
    'email'      : { type: String, required: true, trim: true, unique: true },
    'password'   : { type: String, required: true, trim: true },
    'salt'       : String,
    'meta'       : {
        'createdOn' : { type: Date, default: new Date().toISOString() }
    },
    'groupId'   : [

    ],
    'roles'      : [
        String
    ],
    'accessToken' : String
}