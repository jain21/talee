'use strict';

/**
* Group Model
*/

var mongoose = require('mongoose');
var logger = require('winston');

module.exports = function() {

    var mongooseSchema, schema, Group;

    mongooseSchema = mongoose.Schema;
    schema = require(__dirname + '/schema/group').schema;
    Group = new mongooseSchema(schema);

    mongoose.model('Group', Group);

    if(process.env.NODE_ENV !== 'test'){
        logger.info('Successfully Initialised the Group Schema');
    }
};