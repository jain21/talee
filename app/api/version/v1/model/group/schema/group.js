'use strict';

/**
* Group Schema Document
*/

module.exports.schema = {

    'name' : { type: String, required: true, trim: true, unique: true },
    'meta' : {
        'createdOn' : { type: Date, default: new Date().toISOString() },
        'createdBy' : { type: String, required: true }
    },
    'users' : { type : Array }
}