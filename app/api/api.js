'use strict';

/**
* Request Type
* @type: api
*/

var logger = require('winston');
var error = require(__dirname + '/../../lib/error');

// the middleware function
module.exports = function(request, response, cb) {

    var v, path, version;

    v = request.params.version
    path = __dirname + '/version/'+ v + '/' + v;

    try {
        version = require(path);
        version.route(request, response, function(err, result){
            if(err){
                cb(err);
                return;
            }
            cb(null, result);
        });

    } catch(e) {
        logger.error(e + ' ');
        var err = error['invalid-request'];
        response.writeHead(err.status, { 'Content-Type' : 'application/json' });
        response.end(JSON.stringify(err));
    }
};