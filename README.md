# **Talee** #
###Some form of Social Networking

**Node Version: ** 0.10.28  
**Start App: ** node lib/server.js  
###Directory Structure              
* **lib/**
> contains the express server intialization, imports other modules from the directory, initialize the DB & its schema on server-start
* **middleware/**
> contains the route for API Calls
````https://localhost:8080/:type/:version/:model/:method/````
* **config/**
> ````config.json```` a center-location for a config info for server & db atm; 
> an ````error-code.json```` for common error response code with message; 
> ````exception.json```` for methods that don't require access_token

* **app/**
> The Meat of the application

### WorkFlow

1. Start the server
2. Connect to the MongoDB and initialize all the model schema across all versions
3. For all the requests with the format
````https://localhost:8080/:type/:version/:model/:method/````
use ````middleware/route.js```` to route to the appropriate model and its function and then return the result in the callback
4. so, if the request is valid, it will keeping going down to the correct method and the method would return data in the callback and that callback data goes all the way down to ````route.js```` to actually respond to the request.  
**Note**: Using Try/Catch Blocks if the ````method/model/url```` is not valid, then immediately respond to the request with some error from the ````error.json````

### Example Request
#### ***create(cb)*** ####
Creating a New User and returning access_token + basic user info
##### URL #####
``` javascript
https://api.talee.co/api/v2/user/create.json
```
##### Usage #####
``` javascript
curl -X POST -H "Content-Type: application/json" -d '{"first-name" : "S","last-name" : "J","username" : "jain","email" : "abdc@test.com","password" : "password"}' https://api.talee.co/api/v1/user/create.json
```
##### Response #####

```javascript
{
    "status": {
        "status": 200,
        "error": []
    },
    "body": {
        "id": "53b9aece8a5a5712468ed079",
        "fullname": "S J",
        "username": "jain",
        "role": [
            "regular"
        ],
        "access_token": "<access_token"
    }
}
```