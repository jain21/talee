#!/usr/bin/env bash

REPORTER=spec
ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
TESTPATH="$ROOT/test"

TESTS="$TESTPATH/*/*.test.js"

# result with dot notation
mocha --timeout 3000 ${TESTS}

# result with detailed reporter
# mocha -R ${REPORTER} --timeout 3000 ${TESTS}