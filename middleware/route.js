'use strict';

/**
* For Routing to the correct App Path
*/

var logger = require('winston');
var error = require(__dirname + '/../lib/error');

module.exports = function() {
    return function(req, res, next) {
        var type, path, appType;

        type = req.params.type;
        path = '../app/'+type+'/'+type;

        try {
            appType = require(path);
            appType(req, res, function(err, result) {
                if(err){
                    res.end(JSON.stringify(err));
                    return;
                }
                res.end(JSON.stringify(result));
            })

        } catch(e) {
            logger.error(e + ' ');
            var err = error['invalid-request'];
            res.writeHead(err.status, { 'Content-Type' : 'application/json' });
            res.end(JSON.stringify(err));
        }

        // use next() for the next middleware
    }
};