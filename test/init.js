'use strict';

var mongoose = require('mongoose');
var initDB = require(__dirname+'/../lib/initDB');

process.env.NODE_ENV = 'test';

before(function (done) {
    mongoose.connect('mongodb://localhost/talee_test');
    initDB.loadAll();
    done();
});

after(function(done) {
    mongoose.disconnect();
    console.log('disconnecting DB');
    done();
});


module.exports = {
    before : before,
    after : after
}