var faker = require('faker');

Mocks = {};

Mocks.User = function() {
    return {
        firstName : faker.Name.firstName(),
        lastName : faker.Name.lastName(),
        username : faker.Internet.userName(),
        email: faker.Internet.email(),
        password: 'password'
    };
};

Mocks.staticUser = function() {
    return {
        firstName : 'Test',
        lastName : 'User',
        username : 'testUser',
        email: 'test@testuser.com',
        password: 'secretPass'
    };
};

Mocks.firstName = function() {
    return faker.Name.firstName();
};

Mocks.lastName = function() {
    return faker.Name.lastName();
};

module.exports = Mocks;
