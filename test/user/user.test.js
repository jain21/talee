'use strict';

var init = require('../init');
var should = require('should');
var mongoose = require('mongoose');
var mock = require('../mock');

describe('Create New User', function () {
    var User, access_token;

    before(function(){
        User = require(__dirname + '/../../app/api/version/v1/controller/user/user');
    });

    it('Successfully created a new User', function (done) {
        var data = {
            body : mock.User()
        };

        User.create(data, function(err, res){
            should.not.exist(err);
            res.body.should.have.property('id');
            res.body.should.have.property('fullname');
            res.body.should.have.property('username');
            res.body.should.have.property('role');
            res.body.should.have.property('access_token');
            access_token = res.body.access_token;
            done();
        });
    });

    it('Searched for a user', function(done) {
        var data = {
                body : {
                    q : 's'
                }
            };

        User.search(data, function(err, res){
            should.not.exist(err);
            res.status.should.have.property('status', 200);
            done();
        });
    });

    it('Updated for a user', function(done) {
        var data = {
                access_token : access_token,
                body : {
                    firstName : mock.firstName(),
                    lastName : mock.lastName(),
                    password : '09f903293',
                }
            };

        User.update(data, function(err, res){
            should.not.exist(err);
            res.status.should.have.property('status', 200);
            done();
        });
    });

});